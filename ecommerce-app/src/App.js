import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import Container from 'react-bootstrap/Container';
import './App.css';
import AppNavBar from './components/AppNavBar'
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Error from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import { UserProvider } from './UserContext';

function App(){

    const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
   <UserProvider value={{user, setUser, unsetUser}}>
   <Router>
         <AppNavBar />
         <Container>
           <Switch>
           <Route exact path="/products" component={Products} />
             <Route exact path="/products/:productId" component={ProductView} />
             <Route exact path="/home" component={Home} />
             <Route exact path="/register" component={Register} />
             <Route exact path="/login" component={Login} />
             <Route exact path="/logout" component={Logout} />

             <Route component={Error} />
             
           </Switch>
         </Container>
       </Router>
       </UserProvider>
  )
}

export default App;