import { Fragment } from 'react';
import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';

export default function Home(){

	const data = {
	    title: "Gelato Express",
	    content: "Artisanal Gelato",
	    destination: "/products",
	    label: "Order Now!"
	}


	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}