import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Fragment, useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar() {

const {user} = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand href="#home">Gelato Express</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link as={NavLink} to="/home" exact>Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
		        <Nav.Link as={NavLink} to="/about" exact>About Us</Nav.Link>
		        <Nav.Link as={NavLink} to="/contact" exact>Contact</Nav.Link>
		        { (user.id !== null) ?
		        	<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		        	:
		        	<Fragment>
		        		<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		        		<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		        	</Fragment>
		        } 
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}